export interface Socket
{
    listen (address: string, port: number): void
}
