import { createServer as newHttpServer, Server as HttpServer } from 'http';
import { createServer as newHttpsServer, Server as HttpsServer } from 'https';
import { createSecureServer, Http2SecureServer } from 'http2';
import { readFileSync } from 'fs';

import { Http2ServerWrapper } from './Http2ServerWrapper.js';

const credentials = {
	key: readFileSync('key.pem'),
	cert: readFileSync('cert.pem')
};

export function createHttpsServer (): HttpsServer
{
	return newHttpsServer(credentials);
}

export function createHttp2ServerWrapper (): Http2ServerWrapper
{
	return new Http2ServerWrapper(createHttp2SecureServer(), createHttpServerRedirectToHttps());
}

function createHttp2SecureServer (): Http2SecureServer
{
	return createSecureServer(credentials);
}

function createHttpServerRedirectToHttps (): HttpServer
{
	return newHttpServer((req, res) => {
		res.writeHead(301,{Location: `https://${req.headers.host}${req.url}`});
		res.end();
	});
}
