import { Server } from 'http';
import { constants, Http2SecureServer, IncomingHttpHeaders, ServerHttp2Stream } from 'http2';
import { readFile } from 'fs';
import Mustache from 'mustache';
import { join } from 'path';
import mime from 'mime-types';

const {
	HTTP2_HEADER_PATH,
	HTTP2_HEADER_METHOD,
	HTTP_STATUS_NOT_FOUND,
	HTTP_STATUS_INTERNAL_SERVER_ERROR
} = constants;

const SECURE_PORT = 443;
const UNSECURE_PORT = 80;

export class Http2ServerWrapper {

	private readonly server: Http2SecureServer;

	public constructor(http2SecureServer: Http2SecureServer, httpToHttpsServer: Server)
	{
		this.server = http2SecureServer;
		httpToHttpsServer.listen(UNSECURE_PORT);
	}

	public serve (data: IHomepageTemplateVariables): void {

		this.server.on('stream', (stream: ServerHttp2Stream, headers: IncomingHttpHeaders) => {

			const reqPath = headers[HTTP2_HEADER_PATH] as string;
			const serverRoot = `${process.cwd()}/public/`;
			const fullPath = join(serverRoot, reqPath);
			const responseMimeType = mime.lookup(fullPath);

			if (reqPath === '/') {
				this.loadIndex(data)
					.then((indexHtml) => {
						stream.respond({'content-type': 'text/html; charset=utf-8'});
						stream.end(indexHtml);
					});
			} else {
				stream.respondWithFile(fullPath, {
					'content-type': responseMimeType || 'text/html; charset=utf-8',
				}, {
					onError: (err: NodeJS.ErrnoException) => this.respondToStreamError(err, stream)
				});
			}
		});

		this.server.listen(SECURE_PORT, () => console.log(`Http2SecureServer listening on http://${data.address}`));
	}

	private loadIndex (data: IHomepageTemplateVariables): Promise<string>
	{
		return new Promise<string>((resolve, reject) => {
			readFile(`./public/${data.indexFileName || 'index.html'}`,'utf8', (err, html) => {
				if (err) {
					reject(err);
				}

				resolve(Mustache.render(html, {...data}));
			});
		});
	}

	private respondToStreamError(err: NodeJS.ErrnoException, stream: ServerHttp2Stream) {
		console.log(err);
		if (err.code === 'ENOENT') {
			stream.respond({ ':status': HTTP_STATUS_NOT_FOUND });
		} else {
			stream.respond({ ':status': HTTP_STATUS_INTERNAL_SERVER_ERROR });
		}
		stream.end();
	}
}

export interface IHomepageTemplateVariables
{
	address: string
	indexFileName?: string,
	gamepadSocketPort?: number,
	webcamSocketPort: number
}
