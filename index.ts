import { ControlPanel, ControlPanelOptions } from './ControlPanel/ControlPanel.js';
import { WebcamOptions } from './Webcam/WebcamManager.js';
import { NetworkInterfaces } from './OS/NetworkInterfaces.js';
import 'source-map-support/register.js'

function showHelp () {
	console.log(
`
	Usage: 
	npm start
	
	If you get an error while initializing, pass the ip of the car:
	npm start 192.168.0.23
`
	);
}

const ip = NetworkInterfaces.getIp() || process.argv.slice(2)[0];

if (!ip) {
	showHelp();
	process.exit(1);
}

const webcamOptions: WebcamOptions = {
	width: 960,
	height: 540,
	fps : 12,
};

const controlPanelOptions: ControlPanelOptions = {
	address: ip,
	webcamSocketPort: 10000,
	gamepadSocketPort: 11000,
	webcamOptions
};

new ControlPanel(controlPanelOptions).startComponents();
