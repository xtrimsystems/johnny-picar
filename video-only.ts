import { WebcamSocket } from "./Webcam/WebcamSocket.js";
import { WebcamManager, WebcamOptions } from "./Webcam/WebcamManager.js";
import { NetworkInterfaces } from "./OS/NetworkInterfaces.js";
import { createHttp2ServerWrapper } from "./Server/ServerFactory.js";

function showHelp () {
    console.log(
        `
	Usage: 
	npm start
	
	If you get an error while initializing, pass the ip of the car:
	npm start 192.168.0.23
`
    );
}

const address = NetworkInterfaces.getIp() || process.argv.slice(2)[0];
const webcamSocketPort = 10000;

if (!address) {
    showHelp();
    process.exit(1);
}

const webcamOptions: WebcamOptions = {
    width: 960,
    height: 540,
    fps : 12,
};
const server = createHttp2ServerWrapper();
const webcamSocket = new WebcamSocket(new WebcamManager(webcamOptions, process.cwd()), webcamOptions.width, webcamOptions.height);
const indexFileName = 'index-webcam-only.html'
webcamSocket.listen(address, webcamSocketPort);
server.serve({ address, webcamSocketPort, indexFileName });
