// @ts-ignore
import Splitter from 'stream-split';
import { Readable } from 'stream';
import WS from 'ws';
import { createHttpsServer } from '../Server/ServerFactory.js';
import { WebcamManager } from './WebcamManager.js';
import { Socket } from "../Server/Socket";

const { Server } = WS;
const NALseparator = Buffer.from([0,0,0,1]);

export class WebcamSocket implements Socket
{
	// @ts-ignore
	private socket: Server | undefined;
	private readStream: Readable | undefined;
	private isVideoPaused: boolean

	public constructor(
		private readonly webcamManager: WebcamManager,
		private readonly width: number,
		private readonly height: number,
	) {
		this.isVideoPaused = false;
	}

	public listen (address: string, port: number): void
	{
		this.socket = new Server({
			server: createHttpsServer().listen(
				port,
				address,
				function () {
					// @ts-ignore
					console.log('Webcam socket listening on https://%s:%d', this.address().address, this.address().port);
				}
			)
		});

		// @ts-ignore
		this.socket.on('connection', (client: Server) => this.newClient(client, this.width, this.height));
	}

	private startFeed() {
		if(this.readStream === undefined || this.readStream.destroyed) {
			console.log('Start camera feed')
			this.readStream = this.webcamManager.getFeed();
			this.readStream = this.readStream?.pipe(new Splitter(NALseparator));
			this.readStream?.on('data', (data: any) => this.broadcast(data));
		}
	}

	private broadcast(data: any) {
		this.socket?.clients.forEach(function(socket: any) {
			if((socket as any).buzy)
				return;

			(socket as any).buzy = true;
			(socket as any).buzy = false;

			socket.send(Buffer.concat([NALseparator, data]), { binary: true }, function ack(error: any) {
				(socket as any).buzy = false;
			});
		});
	}

	// @ts-ignore
	private newClient(socket: Server) {

		(socket as any).send(JSON.stringify({
			action : 'init',
			width  : this.width,
			height : this.height,
		}));

		socket.on('message', async (data: string) => {
			if (data.startsWith('data:image/png')) {
				await this.webcamManager.saveScreenshot(data.replace(/^data:image\/png;base64,/, ""));
				return;
			}

			const action = data.split(' ')[0];

			console.log(`Incomming action '%s'`, action);

			if(action === 'REQUESTSTREAM')
				this.startFeed();
			if(action === 'STOPSTREAM') {
				if (this.isVideoPaused) {
					this.readStream?.resume();
					this.isVideoPaused = false;
				} else {
					this.readStream?.pause();
					this.isVideoPaused = true;
				}
			}
		});

		socket.on('close', () => {
			if (this.socket?.clients.size === 0) {
				this.readStream?.destroy();
				this.webcamManager.stopFeed();
				console.log('Stop camera feed');
			}
		});
	}
}
