import { mkdir, stat, writeFile } from 'fs/promises'
import { join } from 'path';
import * as util from 'util';
import { spawn} from 'child_process';

export interface WebcamOptions {
    width: number;
    height: number;
    fps: number;
}

export class WebcamManager
{
    protected options: WebcamOptions;
    private readonly screenshotsPath: string;
    private streamer: any;

    public constructor(options: WebcamOptions, basePath: string)
    {
        this.options = options;
        this.screenshotsPath = join(basePath, 'screenshots');
        this.streamer = null;
    }

    public async saveScreenshot(data: string)
    {
        const dateTime = new Date().toISOString();
        const date = dateTime.split('T')[0];
        const time = dateTime.split('T')[1].split('Z')[0];
        const folderPath = join(this.screenshotsPath, date);

        if (!await this.existsFolder(folderPath)) {
            await mkdir(folderPath);
        }

        writeFile(join(folderPath, `${time}.png`), data, 'base64')
            .then(() => console.log('Image saved'))
            .catch((e) => console.log(e));
    }

    public getFeed() {
        const { width, height, fps } = this.options;
        const msk = 'raspivid -vf -hf -t 0 -o - -w %d -h %d -fps %d -pf baseline';
        const cmd = util.format(msk, width, height, fps);
        const args = cmd.split(' ');

        console.log('Starting', cmd);

        this.streamer = spawn(
            args[0],
            args.splice(1, args.length)
        );

        this.streamer.on('exit', (code: number) => {
            console.log('Failure', code);
        });

        return this.streamer.stdout;
    }

    public stopFeed()
    {
        if (this.streamer) this.streamer.kill('SIGTERM');
    }

    private async existsFolder(folderPath: string): Promise<boolean>
    {
        return stat(folderPath)
            .then((stat) => stat.isDirectory())
            .catch(() => false);
    }
}
