import johnny5 from 'johnny-five';

const { Servo } = johnny5;

export class CustomServo extends Servo
{
	private centerPoint: number | undefined;

	/**
	 * @param value: From an X axe (-1, 0, 1) *Included decimals
	 */
	public turnTo (value: number)
	{
		this.to((value) * (this.range[0] - this.getCenterPoint()) + this.getCenterPoint());
	}

	private getCenterPoint ()
	{
		if (this.centerPoint === undefined) {
			this.centerPoint = ((this.range[1] - this.range[0]) / 2) + this.range[0];
		}

		return this.centerPoint;
	}
}
