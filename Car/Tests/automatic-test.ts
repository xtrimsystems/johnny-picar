// @ts-ignore
import * as sleep from 'system-sleep';

import { Car } from '../Car.js';
import { createCar } from '../CarFactory.js';

const car: Car = createCar();

car.on('car:ready', () => {
	car.centerPanTilt();
	car.turnForward();
	car.stop();

	sleep(500);
	car.turnAllRight();
	sleep(500);
	car.turnForward();
	sleep(500);
	car.turnAllLeft();
	sleep(500);
	car.turnForward();
	sleep(500);

	for(let j=0; j< 40; j++) {
		sleep(100);
		car.panRight();
	}

	sleep(500);
	car.centerPanTilt();

	for(let j=0; j< 40; j++) {
		sleep(100);
		car.panLeft();
	}

	sleep(500);
	car.centerPanTilt();

	for(let j=0; j< 30; j++) {
		sleep(100);
		car.tiltDown();
	}

	sleep(500);
	car.centerPanTilt();

	for(let j=0; j< 30; j++) {
		sleep(100);
		car.tiltUp();
	}

	sleep(500);
	car.centerPanTilt();
});

car.start();
