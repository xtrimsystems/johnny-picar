import { Car } from '../Car.js';
import { createCar } from '../CarFactory.js';
import { emitKeypressEvents } from 'readline';

const car: Car = createCar();

car.on('car:ready', () => {

	emitKeypressEvents(process.stdin);
	process.stdin.setRawMode(true);

	car.centerPanTilt();

	process.stdin.on('keypress', (str, key) => {
		if ( key.sequence === '\u0003' ) {
			car.stop();
			process.exit();
		}

		switch (key.name) {
			case 'left':
				car.panLeft();
				console.log(car.getPanServoPosition());
			break;
			case 'right':
				car.panRight();
				console.log(car.getPanServoPosition());
			break;
			case 'up':
				car.tiltUp();
				console.log(car.getTiltServoPosition());
				break;
			case 'down':
				car.tiltDown();
				console.log(car.getTiltServoPosition());
				break;
			case 'w':
				car.goForward(0.3);
				break;
			case 's':
				car.goReverse(0.3);
				break;
			case 'a':
				car.turnAllLeft();
				break;
			case 'd':
				car.turnAllRight();
				break;
		}
	});

});

car.start();
