import { Board, Motors } from 'johnny-five';
import { EventEmitter } from 'events';

import { createRaspiBoard, createMotors, createFrontWheelsServo, createPanServo, createTiltServo } from './CarFactory.js';
import { CustomServo } from './CustomServo.js';

/**
 * Public Methods:
 * - start(): Gets ready the board and connects all components
 * - stop(): Stops the motors and moves the servos to the center position
 * - goForward(): Moves forward the motors at 'this.speed'
 * - goReverse(): Moves reverse the motors at 'this.speed'
 * - speedUp(): Increases the speed by 1 to a maximum of 240
 * - speed(): Decreases the speed by 1 to a minimum of 40
 * - stopMotors(): Stops the motors
 * - turnLeft(): Moves the front wheels servo to it's minimum
 * - turnRight(): Moves the front wheels servo to it's maximum
 * - turnForward(): Moves the front wheels servo to the center position
 * - panLeft(): Moves the pan servo increasing by 1
 * - panRight(): Moves the pan servo decreasing by 1
 * - panTo(position: number): Moves the pan servo to the position passed if it is synchronize and in range
 * - centerPan(): Moves the pan servo to the center position
 * - tiltUp(): Moves the tilt servo increasing by 1
 * - tiltDown(): Moves the tilt servo decreasing by 1
 * - tiltTo(position: number): Moves the tilt servo to the position passed if it is synchronize and in range
 * - centerTilt(): Moves the tilt servo to the center position
 * - centerPanTilt(): Moves the pan and tilt servos to the center position
 */
export class Car extends EventEmitter
{
	private board: Board;
	private motors: Motors | undefined;
	private frontWheelServo: CustomServo | undefined;
	private panServo: CustomServo | undefined;
	private tiltServo: CustomServo | undefined;
	private readonly MIN_SPEED: number = 40;
	private readonly MAX_SPEED: number = 240;

	public constructor ()
	{
		super();

		this.board = createRaspiBoard();

		this.board.on('exit', () => {
			this.stop();
			this.emit('car:stop');
			console.log('car:stop');
		});
	}

	public start ()
	{
		this.board.on('ready', () => {
			console.log('board:ready');

			this.startComponents();
			// this.startRepl();

			this.emit('car:ready');
			console.log('car:ready');
		});
	}

	public stop ()
	{
		this.stopMotors();
		this.turnForward();
		this.centerPanTilt();
		this.panTo(35);
		this.tiltTo(15);

		this.emit('car:stop');
		console.log('car:stop');
	}

	public goForward (speed: number)
	{
		if (this.motors){
			this.motors.reverse(this.mapSpeedToRange(speed));
		}
	}

	public goReverse (speed: number)
	{
		if (this.motors) {
			this.motors.forward(this.mapSpeedToRange(speed));
		}
	}

	/**
	 * @param speed: speed decimals in between 0 - 1
	 */
	private mapSpeedToRange (speed: number)
	{
		return (speed * this.MAX_SPEED) + this.MIN_SPEED;
	}

	public stopMotors ()
	{
		if (this.motors) {
			this.motors.stop();
		}
	}

	/**
	 * @param value: From an X axe (-1, 0, 1) *Included decimals
	 */
	public turnTo (value: number)
	{
		if (this.frontWheelServo) {
			this.frontWheelServo.turnTo(value);
		}
	}

	public turnAllLeft ()
	{
		if (this.frontWheelServo) {
			this.frontWheelServo.min();
		}
	}

	public turnAllRight ()
	{
		if (this.frontWheelServo) {
			this.frontWheelServo.max();
		}
	}

	public turnForward ()
	{
		if (this.frontWheelServo) {
			this.frontWheelServo.center();
		}
	}

	public panLeft ()
	{
		if (this.panServo) {
			if (this.panServo.position < this.panServo.range[1]) {
				this.panServo.to(this.panServo.position + 1);
			}
		}
	}

	public panRight ()
	{
		if (this.panServo) {
			if (this.panServo.position > this.panServo.range[0]) {
				this.panServo.to(this.panServo.position - 1);
			}
		}
	}

	public panTo (position: number)
	{
		if (this.panServo) {
			this.panServo.turnTo(position);
		}
	}

	public centerPan ()
	{
		if (this.panServo) {
			this.panServo.center();
		}
	}

	public tiltUp ()
	{
		if (this.tiltServo) {
			if (this.tiltServo.position < this.tiltServo.range[1]) {
				this.tiltServo.to(this.tiltServo.position + 1);
			}
		}
	}

	public tiltDown ()
	{
		if (this.tiltServo) {
			if (this.tiltServo.position > this.tiltServo.range[0]) {
				this.tiltServo.to(this.tiltServo.position - 1);
			}
		}
	}

	public tiltTo (position: number)
	{
		if (this.tiltServo) {
			this.tiltServo.turnTo(position);
		}
	}

	public centerTilt ()
	{
		if (this.tiltServo) {
			this.tiltServo.center();
		}
	}

	public centerPanTilt ()
	{
		this.centerPan();
		this.centerTilt();
	}

	public getPanServoPosition (): number
	{
		if (this.panServo) {
			return this.panServo.position;
		}

		throw new Error('Pan servo not initialized');
	}

	public getTiltServoPosition (): number
	{
		if (this.tiltServo) {
			return this.tiltServo.position;
		}

		throw new Error('Tilt servo not initialized');
	}

	private startComponents (): void
	{
		this.motors = createMotors();
		this.frontWheelServo = createFrontWheelsServo();
		this.panServo = createPanServo();
		this.tiltServo = createTiltServo();
	}

	private startRepl (): void
	{
		this.board.repl.inject({
			motors: this.motors,
			frontServo: this.frontWheelServo,
			pan: this.panServo,
			tilt: this.tiltServo
		});
	}
}
