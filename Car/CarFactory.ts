import johnny5 from 'johnny-five';
// @ts-ignore
import raspiIo from 'raspi-io';

const { Board, Motors } = johnny5;

import { Car } from './Car.js';
import { CustomServo } from './CustomServo.js';

const { RaspiIO } = raspiIo;

export function createCar (): Car
{
	return new Car();
}

// @ts-ignore
export function createRaspiBoard (): Board
{
	return new Board({
		io: new RaspiIO(),
		repl: false,
	});
}

export function createFrontWheelsServo (): CustomServo
{
	return new CustomServo({
		pin: 0,
		controller: 'PCA9685',
		range: [ 60, 85 ],
		startAt: 72.5
	});
}

export function createPanServo (): CustomServo
{
	return new CustomServo({
		pin: 1,
		controller: 'PCA9685',
		range: [ 28, 60 ],
		startAt: 35
	});
}

export function createTiltServo (): CustomServo
{
	return new CustomServo({
		pin: 2,
		controller: 'PCA9685',
		range: [ 11, 50 ],
		startAt: 15
	});
}

// @ts-ignore
export function createMotors (): Motors
{
	return new Motors([
		{ controller: 'PCA9685', pins: { pwm: 4, dir: 6 }},
		{ controller: 'PCA9685', pins: { pwm: 5, dir: 7 }}
	]);
}
