import { createCar } from '../Car/CarFactory.js';
import { GamepadSocket } from '../Gamepad/GamepadSocket.js';
import { createHttp2ServerWrapper } from '../Server/ServerFactory.js';
import { WebcamManager, WebcamOptions } from '../Webcam/WebcamManager.js';
import { WebcamSocket } from '../Webcam/WebcamSocket.js';

export interface ControlPanelOptions
{
	address: string;
	gamepadSocketPort: number;
	webcamSocketPort: number;
	webcamOptions: WebcamOptions
}

export class ControlPanel
{
	private readonly options: ControlPanelOptions;

	public constructor (options: ControlPanelOptions)
	{
		this.options = options;
	}

	public startComponents ()
	{
		const { webcamOptions, address, gamepadSocketPort, webcamSocketPort } = this.options
		const car = createCar();

		const server = createHttp2ServerWrapper();

		const gamepadSocket = new GamepadSocket(car);
		const webcamSocket = new WebcamSocket(new WebcamManager(webcamOptions, process.cwd()), webcamOptions.width, webcamOptions.height);

		car.on('car:ready', () => {
			server.serve({ address, gamepadSocketPort, webcamSocketPort });
			gamepadSocket.listen(address, gamepadSocketPort);
			webcamSocket.listen(address, webcamSocketPort);
		});

		car.start();
	}
}
