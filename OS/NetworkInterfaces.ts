import {networkInterfaces} from "os";

export class NetworkInterfaces {

    public static getIp(): string | undefined
    {
        const networks = networkInterfaces();

        if (networks) {
            for (const name of Object.keys(networks)) {
                if (name !== 'wlan0') continue;
                if (networks.hasOwnProperty(name)) {
                    // @ts-ignore
                    for (const net of networks[name]) {
                        if (net.family === 'IPv4' && !net.internal) {
                            return net.address
                        }
                    }
                }
            }
        }
    }
}