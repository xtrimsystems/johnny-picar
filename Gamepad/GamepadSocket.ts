import SocketIO from 'socket.io';

import { Car } from '../Car/Car.js';
import { createHttpsServer } from '../Server/ServerFactory.js';
import { Socket } from '../Server/Socket';

export class GamepadSocket implements Socket
{
	public constructor(private readonly car: Car)
	{
	}

	public listen (address: string, port: number): void
	{
		const io = SocketIO.listen(
			createHttpsServer().listen(
				port,
				address,
				function () {
					// @ts-ignore
					console.log('Gamepad socket listening on https://%s:%d', this.address().address, this.address().port);
				}
			)
		);

		io.sockets.on('connection', (socket: SocketIO.Socket) => {
			socket.on('buttons:state:manual', (buttonsState: any) => {
				console.log(buttonsState.xAxe2, buttonsState.yAxe2)
				if (buttonsState.xAxe) {
					this.car.turnTo(buttonsState.xAxe);
				}

				if (buttonsState.rt === 0 && buttonsState.lt === 0) {
					this.car.stopMotors();
				} else if (buttonsState.rt) {
					this.car.goForward(buttonsState.rt);
				} else if (buttonsState.lt) {
					this.car.goReverse(buttonsState.lt);
				}

				if (buttonsState.xAxe2) {
					this.car.panTo(buttonsState.xAxe2);
				}

				if (buttonsState.yAxe2) {
					this.car.tiltTo(buttonsState.yAxe2);
				}

				if (buttonsState.select) {
					this.car.stop();
				}
			});
		});
	}
}
