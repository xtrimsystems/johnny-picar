# ——— VARIABLES ——————————————————————————————————————————————————————————————————————
ifneq (,$(wildcard ./.env))
	include .env
	export $(shell sed 's/=.*//' .env)
endif

.DEFAULT_GOAL := help

RED := $(shell tput -Txterm setaf 1)
GREEN := $(shell tput -Txterm setaf 2)
BLUE := $(shell tput -Txterm setaf 6)
RESET := $(shell tput -Txterm sgr0)

##—————————————————————————————— BASICS —————————————————————————————————————————————————————

.PHONY: help
help: ## Shows this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##—//' | awk -F ':' '\
    	{ \
			gsub(/^[[:blank:]]+|[[:blank:]]+$$/, "", $$1); \
			if ($$1 ~ /^—+/) { \
				printf "\n$(BLUE)%s$(RESET)\n", $$1; \
			} else { \
				target = $$1; \
				desc = $$2; \
				gsub(/^[[:blank:]]+|[[:blank:]]+$$/, "", desc); \
				split(desc, parts, "\#\#"); \
				dependencies = parts[1]; \
				description = parts[2]; \
				gsub(/^[[:blank:]]+|[[:blank:]]+$$/, "", dependencies); \
				gsub(/^[[:blank:]]+|[[:blank:]]+$$/, "", description); \
				if (dependencies != "") { \
					deplist = " —————> Dependencies: $(RED)" dependencies "$(RESET)"; \
				} else { \
					deplist = ""; \
				} \
				printf "$(GREEN)%-20s$(RESET) %-20s%-30s\n", target, description, deplist; \
			} \
		}'

##—————————————————————————— RUN ON DESKTOP —————————————————————————————————————————————————

.PHONY: init rsync shell

run: ## Run the application without compiling
	ssh $(RASPBERRY_USER)@$(RASPBERRY_IP) 'cd $(PROJECT_PATH_ON_RASPBERRY) && make start'

init: ## Run the application
	ssh $(RASPBERRY_USER)@$(RASPBERRY_IP) 'cd $(PROJECT_PATH_ON_RASPBERRY) && make init-raspberry'

video-only: ## Run the application with only video (as a camera)
	ssh $(RASPBERRY_USER)@$(RASPBERRY_IP) 'cd $(PROJECT_PATH_ON_RASPBERRY) && make video-only-raspberry'

rsync: ## Sync files with the Raspberry Pi
	find $(PROJECT_PATH_ON_DESKTOP) -path $(PROJECT_PATH_ON_DESKTOP)/node_modules -prune -o -path $(PROJECT_PATH_ON_DESKTOP)/.git -prune -o -path $(PROJECT_PATH_ON_DESKTOP)/.idea -prune -o -type f -print | entr -d bash -c 'rsync -avz --exclude-from=$(PROJECT_PATH_ON_DESKTOP)/.rsyncignore $(PROJECT_PATH_ON_DESKTOP) $(RASPBERRY_USER)@$(RASPBERRY_IP):$(PROJECT_PATH_ON_RASPBERRY)'

sync-node_modules: ## Because npm install needs to be run in the Raspberry pi but th eIDE need them, just manually sync them when needed from the Raspberry Pi
	rsync -avz $(RASPBERRY_USER)@$(RASPBERRY_IP):$(PROJECT_PATH_ON_RASPBERRY)/node_modules $(PROJECT_PATH_ON_DESKTOP)/node_modules

shell: ## Ssh to the Raspberry Pi
	ssh $(RASPBERRY_USER)@$(RASPBERRY_IP)

##—————————————————————————— RUN ON RASPBERRY PI ————————————————————————————————————————————

.PHONY: compile start video-only-raspberry manual-test auto-test

init-raspberry: cert.pem node_modules compile start ## Setup the application

node_modules: package.json package-lock.json ## Install node modules
	npm install

cert.pem: key.pem keytmp.pem ## Generate Certificates
	openssl req -nodes -x509 -newkey rsa:2048 -keyout keytmp.pem -out cert.pem -days 365 && openssl rsa -in keytmp.pem -out key.pem

compile: ## Compile source code
	npm run compile

start: ## Start the application
	npm start

video-only-raspberry: ## Start the application with video-only
	npm run video-only

manual-test: ## Run manual test against physical components
	npm run manual-test

auto-test: ## Run automated tests against physical components
	npm run auto-test
